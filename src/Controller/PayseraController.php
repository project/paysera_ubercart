<?php
namespace Drupal\uc_paysera\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\uc_paysera\Service\PayseraSettingsHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Drupal\uc_paysera\Entity\PluginSettings;
use Drupal\uc_paysera\Service\PayseraHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\uc_cart\CartManagerInterface;
use Drupal\uc_order\OrderInterface;
use Drupal\uc_order\Entity\Order;
use UnexpectedValueException;
use Exception;
use WebToPay;
use Drupal;

class PayseraController extends ControllerBase
{
    private $cartManager;

    /**
     * @var PayseraHelper
     */
    private $payseraHelper;

    /**
     * @var PluginSettings
     */
    private $pluginSettings;

    public function __construct(CartManagerInterface $cart_manager)
    {
        $this->cartManager = $cart_manager;
        $this->payseraHelper = Drupal::service('uc_paysera.paysera_helper');
        $this->pluginSettings = PayseraSettingsHelper::getPluginSettings();
    }

    /**
     * @param ContainerInterface $container
     * @return static
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('uc_cart.manager'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function acceptUrl(Request $request)
    {
        $messenger = Drupal::messenger();

        try {
            $response = WebToPay::validateAndParseData(
                $this->payseraHelper->getRequestArray($request),
                $this->pluginSettings->getProjectId(),
                $this->pluginSettings->getProjectSign()
            );

            $orderId = $response['orderid'];

            /** @var OrderInterface $order */
            $order = Order::load($response['orderid']);

            $paymentStatus = $this->pluginSettings->getPaymentStatusNew();

            $this->payseraHelper->setOrderLog($order, t('Customer came back to page'));
            $this->payseraHelper->setOrderLog($order, t('Payment Status') . ': ' . $paymentStatus);

            $order->setStatusId('pending')->save();

            $messenger->addMessage(t('Thank you for your order!'));
            $this->cartManager->completeSale($order);

            return new RedirectResponse($this->payseraHelper->getViewOrderUrl($orderId));
        } catch (Exception $exception) {
            $this->payseraHelper->getPayseraLogger()->warning('acceptUrl exception: ' . $exception->getMessage());

            $messenger->addWarning(t('Error occurred with payment gateway module'));

            return new RedirectResponse('/');
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function notification(Request $request)
    {
        try {
            $response = WebToPay::validateAndParseData(
                $this->payseraHelper->getRequestArray($request),
                $this->pluginSettings->getProjectId(),
                $this->pluginSettings->getProjectSign()
            );

            if ($response['type'] !== 'macro') {
                $this->payseraHelper->getPayseraLogger()->warning(
                    'notification endpoint got a callback with type not equal to macro',
                    [$response]
                );

                throw new UnexpectedValueException('Only macro payment callbacks are accepted');
            }

            $orderId = $response['orderid'];

            /** @var OrderInterface $order */
            $order = Order::load($orderId);

            if ($order !== null) {
                $orderMoney = [
                    'amount' => $this->payseraHelper->convertOrderAmountToString($order->getTotal()),
                    'currency' => $order->getCurrency(),
                ];

                $paymentErrorMessage = $this->checkForPaymentErrorMessage($orderMoney, $response);

                if ($paymentErrorMessage === null) {
                    if ($response['status'] === '1') {
                        $paymentStatus = $this->pluginSettings->getPaymentStatusConfirmed();

                        if ($order->getStateId() !== 'payment_received') {
                            $this->payseraHelper->setOrderLog($order, t('Callback payment completed'));
                            $this->payseraHelper->setOrderLog($order, t('Payment Status') . ': ' . $paymentStatus);

                            $this->payseraHelper->createPayment($order->id(), $response, $order->getOwnerId());
                        }

                        echo 'OK, payment was initiated';
                    }
                } else {
                    echo $paymentErrorMessage;
                }
            } else {
                echo 'ERROR, order does not exists';
            }
        } catch (Exception $exception) {
            echo 'EXCEPTION, ' . $exception->getMessage();

            $this->payseraHelper->getPayseraLogger()->warning('Callback exception: ' . $exception->getMessage());
        }

        return new Response('');
    }

    /**
     * @param array $orderMoney
     * @param array $response
     * @return string|null
     */
    private function checkForPaymentErrorMessage(array $orderMoney, array $response)
    {
        $orderAmount = $orderMoney['amount'];
        $orderCurrency = $orderMoney['currency'];

        if ($response['amount'] !== $orderAmount || $response['currency'] !== $orderCurrency) {
            $checkConvert = array_key_exists('payamount', $response);

            if (!$checkConvert) {
                return (
                    'Wrong amount: ' . $response['amount'] / 100 . $response['currency']
                    . ', expected: ' . $orderAmount / 100 . $orderCurrency
                );
            } elseif ($response['payamount'] !== $orderAmount || $response['paycurrency'] !== $orderCurrency) {
                return (
                    'Wrong pay amount: ' . $response['payamount'] / 100 . $response['paycurrency']
                    . ', expected: ' . $orderAmount / 100 . $orderCurrency
                );
            }
        }

        return null;
    }
}
