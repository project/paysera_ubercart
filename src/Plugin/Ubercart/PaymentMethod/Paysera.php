<?php
namespace Drupal\uc_paysera\Plugin\Ubercart\PaymentMethod;

use Drupal\uc_paysera\Plugin\Ubercart\CheckoutPane\PayseraOrderFields;
use Drupal\uc_payment\OffsitePaymentMethodPluginInterface;
use Drupal\uc_payment\Annotation\UbercartPaymentMethod;
use Drupal\uc_paysera\Service\PayseraSettingsHelper;
use Drupal\uc_payment\PaymentMethodPluginBase;
use Drupal\uc_paysera\Entity\PluginSettings;
use Drupal\uc_paysera\Service\PayseraHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\uc_order\OrderInterface;
use Drupal\uc_store\Address;
use WebToPayException;
use WebToPay_Factory;
use Drupal;

/**
 * Defines the Paysera payment method.
 *
 * @UbercartPaymentMethod(
 *  id = "Paysera",
 *  name = @Translation("Paysera"),
 *  display_label = @Translation("All popular payment methods"),
 * )
 */
class Paysera extends PaymentMethodPluginBase implements OffsitePaymentMethodPluginInterface
{
    /**
     * @var PluginSettings
     */
    private $pluginSettings;

    /**
     * @var PayseraHelper
     */
    private $payseraHelper;
    private $payseraOrderFields;

    public function __construct(array $configuration, $plugin_id, $plugin_definition)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);

        $this->pluginSettings = PayseraSettingsHelper::getPluginSettings();
        $this->payseraHelper = Drupal::service('uc_paysera.paysera_helper');
        $this->payseraOrderFields = new PayseraOrderFields();
    }

    /**
     * @param string $label
     * @return array
     */
    public function getDisplayLabel($label)
    {
        $pane_form['label'] = [
            '#plain_text' => $label,
            '#suffix' => '<br/>',
        ];

        $pane_form['image'] = [
            '#theme' => 'image',
            '#uri' => drupal_get_path('module', 'uc_paysera') . '/images/paysera-logo.png',
            '#alt' => 'paysera',
            '#attributes' => ['class' => ['uc-paysera-logo']],
            '#suffix' => '<br/>',
        ];

        return $pane_form;
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form['main_settings'] = [];

        $form['extra_settings'] = [
            '#type' => 'details',
            '#title' => t('Extra Settings'),
        ];

        $form['order_statuses'] = [
            '#type' => 'details',
            '#title' => t('Payment Status'),
        ];

        $form['project_additional'] = [
            '#type' => 'details',
            '#title' => t('Project additions'),
        ];

        $form['main_settings']['project_id'] = [
            '#type' => 'textfield',
            '#default_value' => $this->pluginSettings->getProjectId(),
            '#title' => t('Project ID'),
            '#description' => t('Project id'),
            '#required' => true,
        ];

        $form['main_settings']['project_sign'] = [
            '#type' => 'textfield',
            '#default_value' => $this->pluginSettings->getProjectSign(),
            '#title' => t('Sign'),
            '#description' => t('Paysera project sign password'),
            '#required' => true,
        ];

        $form['main_settings']['test_mode'] = [
            '#type' => 'checkbox',
            '#default_value' => $this->pluginSettings->isTestModeEnabled(),
            '#title' => t('Test Mode'),
            '#description' => t('Enable this if you want to make test payments'),
        ];

        $form['extra_settings']['checkout_description'] = [
            '#type' => 'textarea',
            '#default_value' => t($this->pluginSettings->getCheckoutDescription()),
            '#title' => t('Description'),
            '#cols' => 100,
            '#rows' => 3,
            '#description' => t('This controls the description which the user sees during checkout.'),
            '#attributes' => [
                'size' => '60',
                'style' => 'width:50%;',
            ],
        ];

        $form['extra_settings']['payment_list_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('List of payments'),
            '#description' => t('Enable this to display payment methods list at checkout page'),
            '#default_value' => $this->pluginSettings->isPaymentListEnabled(),
        ];

        $form['extra_settings']['enabled_countries'] = [
            '#type' => 'select',
            '#title' => t('Specific countries'),
            '#options' => $this->payseraHelper->getProjectCountries($this->pluginSettings->getProjectId()),
            '#description' => t('Select which country payment methods to display (empty means all)'),
            '#size' => 5,
            '#default_value' => $this->pluginSettings->getEnabledCountries(),
            '#multiple' => true,
            '#attributes' => [
                'style' => 'width:50%;',
            ],
        ];

        $form['extra_settings']['grid_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('Grid view'),
            '#description' => t('Enable this to use payment methods grid view'),
            '#default_value' => $this->pluginSettings->isGridEnabled(),
        ];

        $form['extra_settings']['buyer_consent'] = [
            '#type' => 'checkbox',
            '#title' => t('Buyer consent'),
            '#description' => t('Enable this to skip the additional step when using PIS payment methods'),
            '#default_value' => $this->pluginSettings->isBuyerConsentEnabled(),
        ];

        $form['order_statuses']['payment_status_new'] = [
            '#type' => 'select',
            '#title' => t('New Payment Status'),
            '#options' => $this->payseraHelper->getAvailablePaymentStatuses(),
            '#default_value' => $this->pluginSettings->getPaymentStatusNew(),
            '#attributes' => [
                'style' => 'width:50%;height:28px;',
            ],
        ];

        $form['order_statuses']['payment_status_confirmed'] = [
            '#type' => 'select',
            '#title' => t('Paid Payment Status'),
            '#options' => $this->payseraHelper->getAvailablePaymentStatuses(),
            '#default_value' => $this->pluginSettings->getPaymentStatusConfirmed(),
            '#attributes' => [
                'style' => 'width:50%;height:28px;',
            ],
        ];

        $form['order_statuses']['payment_status_pending'] = [
            '#type' => 'select',
            '#title' => t('Pending checkout'),
            '#options' => $this->payseraHelper->getAvailablePaymentStatuses(),
            '#default_value' => $this->pluginSettings->getPaymentStatusPending(),
            '#attributes' => [
                'style' => 'width:50%;height:28px;',
            ],
        ];

        $form['project_additional']['quality_sign_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('Quality sign'),
            '#description' => t('Enable this to use quality sign'),
            '#default_value' => $this->pluginSettings->isQualitySignEnabled(),
        ];

        $form['project_additional']['ownership_code_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('Ownership of website'),
            '#description' => t('Enable this to place ownership code'),
            '#default_value' => $this->pluginSettings->isOwnershipCodeEnabled(),
        ];

        $form['project_additional']['ownership_code'] = [
            '#type' => 'textfield',
            '#title' => t('Ownership code'),
            '#default_value' => $this->pluginSettings->getOwnershipCode(),
        ];

        return $form;
    }

    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        if (filter_var($form['main_settings']['project_id']['#value'], FILTER_VALIDATE_INT) === false) {
            $form_state->setErrorByName('project_id', ($this->t('Project ID') . ' ' . $this->t('not valid')));
        }

        if (
            (bool) $form['project_additional']['ownership_code_mode']['#value']
            && empty($form['project_additional']['ownership_code']['#value'])
        ) {
            $form_state->setErrorByName('ownership_code', ($this->t('Ownership code')  . ' ' .  $this->t('not valid')));
        }

        return parent::validateConfigurationForm($form, $form_state);
    }

    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValue($form['#parents']);

        PayseraSettingsHelper::getPayseraConfig()
            ->set('project_id', $values['main_settings']['project_id'])
            ->set('project_sign', $values['main_settings']['project_sign'])
            ->set('test_mode', $values['main_settings']['test_mode'])
            ->set('checkout_description', $values['extra_settings']['checkout_description'])
            ->set('payment_list_mode', $values['extra_settings']['payment_list_mode'])
            ->set('enabled_countries', $values['extra_settings']['enabled_countries'])
            ->set('grid_mode', $values['extra_settings']['grid_mode'])
            ->set('buyer_consent', $values['extra_settings']['buyer_consent'])
            ->set('payment_status_new', $values['order_statuses']['payment_status_new'])
            ->set('payment_status_confirmed', $values['order_statuses']['payment_status_confirmed'])
            ->set('payment_status_pending', $values['order_statuses']['payment_status_pending'])
            ->set('quality_sign_mode', $values['project_additional']['quality_sign_mode'])
            ->set('ownership_code_mode', $values['project_additional']['ownership_code_mode'])
            ->set('ownership_code', $values['project_additional']['ownership_code'])
            ->save()
        ;

        parent::submitConfigurationForm($form, $form_state);
    }

    public function cartProcess(OrderInterface $order, array $form, FormStateInterface $form_state)
    {
        $userInput = $form_state->getUserInput();

        $paymentCountry = isset($userInput['paysera_payment_country']) ? $userInput['paysera_payment_country'] : null;
        $paymentMethod = isset($userInput['paysera_payment_radio']) ? $userInput['paysera_payment_radio'] : null;

        $session = Drupal::service('session');
        $session->set('paysera_payment_country', $paymentCountry);
        $session->set('paysera_payment_method', $paymentMethod);
    }

    /**
     * @param OrderInterface $order
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function cartDetails(OrderInterface $order, array $form, FormStateInterface $form_state)
    {
        return $this->payseraOrderFields->buildPaneForm();
    }

    /**
     * @return string
     */
    public function cartReviewTitle()
    {
        $session = Drupal::service('session');

        if ($session->get('paysera_payment_method') !== null) {
            $paymentMethod = 'Paysera: ' . $session->get('paysera_payment_method');
        } else {
            $paymentMethod = 'Paysera';
        }

        return $this->t((string) $paymentMethod);
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @param OrderInterface|null $order
     * @return array
     */
    public function buildRedirectForm(array $form, FormStateInterface $form_state, OrderInterface $order = null)
    {
        $session = Drupal::service('session');
        $requestParameters = [];

        /** @var Address $billingAddress */
        $billingAddress = $order->getAddress('billing');

        $requestParameters['p_firstname'] = $billingAddress->getFirstName();
        $requestParameters['p_lastname'] = $billingAddress->getLastName();
        $requestParameters['p_street'] = $billingAddress->getStreet1();
        $requestParameters['p_countrycode'] = $billingAddress->getCountry();
        $requestParameters['p_zip'] = $billingAddress->getPostalCode();

        try {
            $requestParameters['amount'] = $this->payseraHelper->convertOrderAmountToString($order->getTotal());
            $requestParameters['lang'] = $this->payseraHelper->drupalLanguageToPaysera(
                $this->payseraHelper->getCurrentLanguage()
            );
            $requestParameters['projectid'] = $this->pluginSettings->getProjectId();
            $requestParameters['sign_password'] = $this->pluginSettings->getProjectSign();
            $requestParameters['orderid'] = $order->id();
            $requestParameters['currency'] = $order->getCurrency();
            $requestParameters['p_email'] = $order->getEmail();
            $requestParameters['country'] = $session->get('paysera_payment_country');
            $requestParameters['payment'] = $session->get('paysera_payment_method') ?? 'paysera';
            $requestParameters['accepturl'] = $this->payseraHelper->getBaseUrl() . 'paysera_accept/';
            $requestParameters['cancelurl'] = $this->payseraHelper->getBaseUrl() . 'cart/checkout/review/';
            $requestParameters['callbackurl'] = $this->payseraHelper->getBaseUrl() . 'cart/paysera/notification/';
            $requestParameters['test'] = (int) $this->pluginSettings->isTestModeEnabled();
            $requestParameters['buyer_consent'] = (int) $this->pluginSettings->isBuyerConsentEnabled();

            $paymentStatus = $this->pluginSettings->getPaymentStatusPending();

            $this->payseraHelper->setOrderLog($order, t('Payment checkout process is started'));
            $this->payseraHelper->setOrderLog($order, t('Payment Status') . ': ' . $paymentStatus);

            $redirectUrl = (new WebToPay_Factory(
                [
                    'projectId' => $this->pluginSettings->getProjectId(),
                    'password' => $this->pluginSettings->getProjectSign(),
                ]
            ))
                ->getRequestBuilder()
                ->buildRequestUrlFromData($requestParameters)
            ;
        } catch (WebToPayException $exception) {
            $this->payseraHelper->getPayseraLogger()->warning(
                'Form builder, webtopay exception',
                ['exception' => $exception]
            );

            Drupal::messenger()->addWarning(t('Error occurred with payment gateway module'));

            $redirectUrl = $this->payseraHelper->getBaseUrl();
        }

        $form['#action'] = $redirectUrl;
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit order'),
        ];

        return $form;
    }
}
