<?php
namespace Drupal\uc_paysera\Plugin\Ubercart\CheckoutPane;

use Drupal\uc_paysera\Service\PayseraSettingsHelper;
use Drupal\uc_paysera\Service\PayseraHelper;
use Drupal\uc_paysera\Entity\PluginSettings;
use Drupal\Core\Annotation\Translation;
use Drupal\uc_order\Entity\Order;
use WebToPay_PaymentMethodList;
use Drupal\uc_store\Address;
use WebToPay_PaymentMethod;
use WebToPayException;
use WebToPay;
use Drupal;

/**
 * @PayseraOrderFields(
 *  id = "paysera_order_fields",
 *  label = @Translation("Paysera paysera order fields"),
 *  admin_label = @Translation("Paysera paysera order fields"),
 *  display_label = @Translation("All popular payment methods"),
 *  default_step = "review",
 * )
 */
class PayseraOrderFields
{
    const GRID_VIEW_ROWS_NUMBER = 3;

    /**
     * @var PayseraHelper
     */
    private $payseraHelper;

    /**
     * @var PluginSettings
     */
    private $pluginSettings;

    public function __construct()
    {
        $this->payseraHelper = Drupal::service('uc_paysera.paysera_helper');
        $this->pluginSettings = PayseraSettingsHelper::getPluginSettings();
    }

    /**
     * @return array
     */
    public function buildPaneForm()
    {
        $pane_form = [];

        $session = Drupal::service('session');
        $order = Order::load($session->get('cart_order'));

        /** @var $billingAddress Address */
        $billingAddress = $order->getAddress('billing');
        $billingCountry = strtolower($billingAddress->getCountry()) ?? 'unknown';

        $pane_form['#attached']['library'][] = 'uc_paysera/paysera-checkout';

        if (count($this->pluginSettings->getCheckoutDescription()) > 0) {
            $pane_form['paysera_checkout']['checkout_description'] = [
                '#markup' => $this->pluginSettings->getCheckoutDescription(),
            ];
        }

        if ($this->pluginSettings->isPaymentListEnabled()) {
            try {
                $amount = $this->payseraHelper->convertOrderAmountToString($order->getTotal());
                $currency = $order->getCurrency();

                $paymentMethodsInfo = WebToPay::getPaymentMethodList(
                    $this->pluginSettings->getProjectId(),
                    $order->getCurrency()
                )
                    ->setDefaultLanguage($this->payseraHelper->getCurrentLanguage())
                    ->filterForAmount($amount, $currency)
                ;
            } catch (WebToPayException $exception) {
                $this->payseraHelper->getPayseraLogger()->warning(
                    'buildPaneForm payment list exception: ' . $exception->getMessage()
                );

                return $pane_form;
            }

            $activeCountry = function () use ($paymentMethodsInfo, $billingCountry) {
                $enabledCountries = $this->pluginSettings->getEnabledCountries();

                if (count($this->pluginSettings->getEnabledCountries()) > 0) {
                    if (!in_array($billingCountry, $enabledCountries, true)) {
                        return $enabledCountries[0];
                    }
                }

                foreach ($paymentMethodsInfo->getCountries() as $country) {
                    if ($country->getCode() === $billingCountry) {
                        return $billingCountry;
                    }
                }

                return 'other';
            };


            $pane_form['paysera_checkout']['payment_country'] = [
                '#id' => 'paysera_payment_country',
                '#name' => 'paysera_payment_country',
                '#type' => 'select',
                '#options' => $this->payseraHelper->getEnabledCountries($this->pluginSettings),
                '#default_value' => $activeCountry(),
                '#attributes' => [
                    'style' => 'width:70%;',
                    'onchange' => 'payseraCountryChange()',
                ],
            ];

            $pane_form['paysera_checkout']['payment_method'] = [
                '#type' => 'hidden',
                '#attributes' => [
                    'id' => 'paysera_payment_method',
                ],
            ];

            if ($this->pluginSettings->isGridEnabled()) {
                $pane_form[] = $this->generateGridView($paymentMethodsInfo, $activeCountry());
            } else {
                $pane_form[] = $this->generateListView($paymentMethodsInfo, $activeCountry());
            }
        }

        if ($this->pluginSettings->isBuyerConsentEnabled()) {
            $buyerConsent = '</br>';

            $buyerConsent .= sprintf(
                t('Please be informed that the account information and payment initiation services will be provided to you by Paysera in accordance with these %s. By proceeding with this payment, you agree to receive this service and the service terms and conditions.'),
                '<a href="' . t('https://www.paysera.lt/v2/en-LT/legal/pis-rules-2020') . '"> '
                . t('rules')  .'</a>'
            );

            $pane_form['paysera_buyer_consent'] = [
                '#markup' => $buyerConsent,
            ];
        }

        return $pane_form;
    }

    /**
     * @param WebToPay_PaymentMethodList $paymentMethodsInfo
     * @param string $activeCountry
     * @return array
     */
    private function generateListView(WebToPay_PaymentMethodList $paymentMethodsInfo, string $activeCountry)
    {
        $form = [];

        foreach ($paymentMethodsInfo->getCountries() as $country) {
            $fieldClasses = 'paysera-methods ' . ($activeCountry === $country->getCode() ? '' : 'hide-payment-country');

            $form['paysera_checkout'][$country->getCode()] = [
                '#id' => 'paysera_payment_country_' . $country->getCode(),
                '#type' => 'fieldset',
                '#title' => $country->getTitle(),
                '#attributes' => [
                    'class' => $fieldClasses,
                ],
            ];

            foreach ($country->getGroups() as $group) {
                $form['paysera_checkout'][$country->getCode()][$group->getKey()] = [
                    '#id' => 'paysera-payment',
                    '#type' => 'markup',
                    '#markup' => $group->getTitle(),
                ];

                foreach ($group->getPaymentMethods() as $paymentMethod) {
                    $form['paysera_checkout'][$country->getCode()][$group->getKey()][$paymentMethod->getKey()][] = [
                        '#type' => 'radio',
                        '#name' => 'paysera_payment_radio',
                        '#title' => $paymentMethod->getTitle() . ' <img src="' . $paymentMethod->getLogoUrl() . '" />',
                        '#attributes' => [
                            'value' => $paymentMethod->getKey(),
                        ],
                    ];
                }
            }
        }

        return $form;
    }

    /**
     * @param WebToPay_PaymentMethodList $paymentMethodsInfo
     * @param string $activeCountry
     * @return array
     */
    private function generateGridView(WebToPay_PaymentMethodList $paymentMethodsInfo, string $activeCountry)
    {
        $form = [];

        foreach ($paymentMethodsInfo->getCountries() as $country) {
            $fieldClasses = 'paysera-methods ' . ($activeCountry === $country->getCode() ? '' : 'hide-payment-country');

            $form['paysera_checkout'][$country->getCode()] = [
                '#id' => 'paysera_payment_country_' . $country->getCode(),
                '#type' => 'fieldset',
                '#title' => $country->getTitle(),
                '#attributes' => [
                    'class' => $fieldClasses,
                ],
            ];

            foreach ($country->getGroups() as $group) {
                $form['paysera_checkout'][$country->getCode()][$group->getKey()] = [
                    '#type' => 'table',
                    '#caption' => $group->getTitle(),
                ];

                $methods = $group->getPaymentMethods();

                $methodsRowsArray = [];
                $methodsCount = count($methods);
                $loopCount = 0;

                while ($loopCount < $methodsCount) {
                    $methodsRowsArray[] = array_slice($methods, $loopCount, self::GRID_VIEW_ROWS_NUMBER);

                    $loopCount += self::GRID_VIEW_ROWS_NUMBER;
                }

                foreach ($methodsRowsArray as $rowId => $threeMethodsArray) {
                    foreach ($threeMethodsArray as $paymentId => $payment) {
                        /** @var $payment WebToPay_PaymentMethod */
                        $form['paysera_checkout'][$country->getCode()][$group->getKey()][$rowId]['payment_row_' . $paymentId] = [
                            '#type' => 'radio',
                            '#name' => 'paysera_payment_radio',
                            '#title' => '<img src="' . $payment->getLogoUrl() . '" />',
                            '#attributes' => [
                                'value' => $payment->getKey(),
                            ],
                        ];
                    }
                }
            }
        }

        return $form;
    }
}
