<?php
namespace Drupal\uc_paysera\Entity;

class PluginSettings
{
    /**
     * @var int
     */
    private $projectId;

    /**
     * @var string
     */
    private $projectSign;

    /**
     * @var bool
     */
    private $testMode;

    /**
     * @var string
     */
    private $checkoutDescription;

    /**
     * @var bool
     */
    private $paymentListEnabled;

    /**
     * @var array
     */
    private $enabledCountries;

    /**
     * @var bool
     */
    private $gridEnabled;

    /**
     * @var bool
     */
    private $buyerConsentEnabled;

    /**
     * @var string
     */
    private $paymentStatusNew;

    /**
     * @var string
     */
    private $paymentStatusConfirmed;

    /**
     * @var string
     */
    private $paymentStatusPending;

    /**
     * @var bool
     */
    private $qualitySignEnabled;

    /**
     * @var bool
     */
    private $ownershipCodeEnabled;

    /**
     * @var string
     */
    private $ownershipCode;

    public function __construct()
    {
        $this->enabledCountries = [];
    }

    /**
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param int $projectId
     * @return self
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjectSign()
    {
        return $this->projectSign;
    }

    /**
     * @param string $projectSign
     * @return self
     */
    public function setProjectSign($projectSign)
    {
        $this->projectSign = $projectSign;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckoutDescription()
    {
        return $this->checkoutDescription;
    }

    /**
     * @param string $checkoutDescription
     * @return self
     */
    public function setCheckoutDescription($checkoutDescription)
    {
        $this->checkoutDescription = $checkoutDescription;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaymentListEnabled()
    {
        return $this->paymentListEnabled;
    }

    /**
     * @return bool
     */
    public function isTestModeEnabled()
    {
        return $this->testMode;
    }

    /**
     * @param bool $testMode
     * @return self
     */
    public function setTestMode($testMode)
    {
        $this->testMode = $testMode;
        return $this;
    }

    /**
     * @param bool $paymentListEnabled
     * @return self
     */
    public function setPaymentListEnabled($paymentListEnabled)
    {
        $this->paymentListEnabled = $paymentListEnabled;
        return $this;
    }

    /**
     * @return array
     */
    public function getEnabledCountries()
    {
        return $this->enabledCountries;
    }

    /**
     * @param string $country
     * @return self
     */
    public function addEnabledCountry($country)
    {
        $this->enabledCountries[] = $country;
        return $this;
    }

    /**
     * @param array $enabledCountries
     * @return self
     */
    public function setEnabledCountries(array $enabledCountries)
    {
        $this->enabledCountries = $enabledCountries;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGridEnabled()
    {
        return $this->gridEnabled;
    }

    /**
     * @param bool $gridEnabled
     * @return self
     */
    public function setGridEnabled($gridEnabled)
    {
        $this->gridEnabled = $gridEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBuyerConsentEnabled()
    {
        return $this->buyerConsentEnabled;
    }

    /**
     * @param bool $buyerConsentEnabled
     * @return self
     */
    public function setBuyerConsentEnabled($buyerConsentEnabled)
    {
        $this->buyerConsentEnabled = $buyerConsentEnabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatusNew()
    {
        return $this->paymentStatusNew;
    }

    /**
     * @param string $paymentStatusNew
     * @return self
     */
    public function setPaymentStatusNew($paymentStatusNew)
    {
        $this->paymentStatusNew = $paymentStatusNew;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatusConfirmed()
    {
        return $this->paymentStatusConfirmed;
    }

    /**
     * @param string $paymentStatusConfirmed
     * @return self
     */
    public function setPaymentStatusConfirmed($paymentStatusConfirmed)
    {
        $this->paymentStatusConfirmed = $paymentStatusConfirmed;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatusPending()
    {
        return $this->paymentStatusPending;
    }

    /**
     * @param string $paymentStatusPending
     * @return self
     */
    public function setPaymentStatusPending($paymentStatusPending)
    {
        $this->paymentStatusPending = $paymentStatusPending;
        return $this;
    }

    /**
     * @return bool
     */
    public function isQualitySignEnabled()
    {
        return $this->qualitySignEnabled;
    }

    /**
     * @param bool $qualitySignEnabled
     * @return self
     */
    public function setQualitySignEnabled($qualitySignEnabled)
    {
        $this->qualitySignEnabled = $qualitySignEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOwnershipCodeEnabled()
    {
        return $this->ownershipCodeEnabled;
    }

    /**
     * @param bool $ownershipCodeEnabled
     * @return self
     */
    public function setOwnershipCodeEnabled($ownershipCodeEnabled)
    {
        $this->ownershipCodeEnabled = $ownershipCodeEnabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipCode()
    {
        return $this->ownershipCode;
    }

    /**
     * @param string $ownershipCode
     * @return self
     */
    public function setOwnershipCode($ownershipCode)
    {
        $this->ownershipCode = $ownershipCode;
        return $this;
    }
}
