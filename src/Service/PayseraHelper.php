<?php
namespace Drupal\uc_paysera\Service;

use Symfony\Component\HttpFoundation\Request;
use Drupal\uc_paysera\Entity\PluginSettings;
use Drupal\uc_order\OrderInterface;
use Psr\Log\LoggerInterface;
use WebToPayException;
use WebToPay;
use Drupal;

class PayseraHelper
{
    const PAYSERA_UC_MACHINE_PLUGIN_NAME = 'paysera_uc_payment_gateway';
    const PAYSERA_SETTINGS_NAME = 'uc_paysera.settings';
    const PAYSERA_UC_NAME = 'uc_paysera';
    const PAYSERA_UC_TRANSLATION_DIR = 'translations';
    const PAYSERA_UC_TEMPLATES_DIR = 'templates';

    /**
     * @return LoggerInterface
     */
    public function getPayseraLogger()
    {
        return Drupal::logger(self::PAYSERA_UC_NAME);
    }

    /**
     * @param PluginSettings $pluginSettings
     * @return array
     */
    public function getEnabledCountries(PluginSettings $pluginSettings)
    {
        $enabledCountries = $pluginSettings->getEnabledCountries();

        if (count($enabledCountries) > 0) {
            $projectCountries = $this->getProjectCountries($pluginSettings->getProjectId());
            $translatedEnabledCountries = [];

            if (count($projectCountries) > 0) {
                foreach ($projectCountries as $countryCode => $countryTitle) {
                    if (in_array($countryCode, $enabledCountries, true)) {
                        $translatedEnabledCountries[$countryCode] = $countryTitle;
                    }
                }
            }

            return $translatedEnabledCountries;
        }

        return $this->getProjectCountries($pluginSettings->getProjectId());
    }

    /**
     * @param int $projectId
     * @return array
     */
    public function getProjectCountries(int $projectId)
    {
        try {
            $paymentMethods = WebToPay::getPaymentMethodList($projectId, $this->getDefaultSiteCurrency())
                ->setDefaultLanguage($this->getCurrentLanguage())
                ->getCountries()
            ;
        } catch (WebToPayException $exception) {
            $this->getPayseraLogger()->warning('Get project countries exception: ' . $exception->getMessage());
            return [];
        }

        $availableCountries = [];

        foreach ($paymentMethods as $country) {
            $availableCountries[$country->getCode()] = $country->getTitle();
        }

        return $availableCountries;
    }

    public function setOrderLog(OrderInterface $order, string $comment)
    {
        $order->logChanges(
            [t('Paysera: @comment', ['@comment' => $comment])]
        );
    }

    /**
     * @return string
     */
    public function getDefaultSiteCurrency()
    {
        return Drupal::config('uc_store.settings')->get('currency')['code'] ?? 'EUR';
    }

    /**
     * @return string
     */
    public function getCurrentLanguage()
    {
        return Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    /**
     * @return array
     */
    public function getAvailablePaymentStatuses()
    {
        return [
            'new' => t('New'),
            'authorization' => t('Authorization'),
            'completed' => t('Completed'),
        ];
    }

    /**
     * @param string $drupalLanguage
     * @return string
     */
    public function drupalLanguageToPaysera(string $drupalLanguage)
    {
        switch ($drupalLanguage) {
            case 'en':
                return 'ENG';
            case 'lt':
                return 'LIT';
            case 'lv':
                return 'LAV';
            case 'es':
                return 'EST';
            case 'ru':
                return 'RUS';
            case 'de':
                return 'GER';
            case 'pl':
                return 'POL';
            default: return '';
        }
    }

    /**
     * @param array $response
     * @return int
     */
    public function formatAmountFromPayseraResponse(array $response)
    {
        $checkConvert = array_key_exists('payamount', $response);

        return ($checkConvert ? $response['payamount'] : $response['amount']) / 100;
    }

    public function createPayment(string $orderId, array $response, string $ownerId)
    {
        uc_payment_enter(
            $orderId,
            'paysera',
            $this->formatAmountFromPayseraResponse($response),
            $ownerId,
            null,
            'payed by: ' . $response['payment']
        );
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getRequestArray(Request $request)
    {
        return array_merge($request->query->all(), $request->request->all());
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return Drupal::request()->getSchemeAndHttpHost() . '/';
    }

    /**
     * @param float $amount
     * @return string
     */
    public function convertOrderAmountToString(float $amount)
    {
        return (string) (round($amount, 2) * 100);
    }

    /**
     * @param string $orderId
     * @return string
     */
    public function getViewOrderUrl(string $orderId)
    {
        $userId = Drupal::currentUser()->id();

        return $userId !== 0 ? ('/user/' . $userId . '/orders/' . $orderId) : '';
    }
}
