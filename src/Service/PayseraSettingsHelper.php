<?php
namespace Drupal\uc_paysera\Service;

use Drupal\uc_paysera\Normalizer\PluginSettingsNormalizer;
use Drupal\uc_paysera\Entity\PluginSettings;
use Drupal\Core\Config\Config;
use Drupal;

class PayseraSettingsHelper
{
    /**
     * @return PluginSettings
     */
    public static function getPluginSettings()
    {
        return (new PluginSettingsNormalizer())
            ->normalizeProjectSettings(Drupal::config(PayseraHelper::PAYSERA_SETTINGS_NAME)
        );
    }

    /**
     * @return Config
     */
    public static function getPayseraConfig()
    {
        return Drupal::configFactory()->getEditable(PayseraHelper::PAYSERA_SETTINGS_NAME);
    }

    /**
     * @return array
     */
    public static function loadDefaultSettings()
    {
        return [
            'project_id' => 1,
            'project_sign' => '',
            'test_mode' => false,
            'checkout_description' => 'Choose a payment method on the Paysera page',
            'payment_list_mode' => false,
            'enabled_countries' => [],
            'grid_mode' => false,
            'buyer_consent' => true,
            'payment_status_new' => 'authorization',
            'payment_status_confirmed' => 'completed',
            'payment_status_pending' => 'new',
            'quality_sign_mode' => false,
            'ownership_code_mode' => false,
            'ownership_code' => '',
        ];
    }
}
