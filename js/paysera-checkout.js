document.addEventListener('DOMContentLoaded', function() {
    let payseraPaymentRadio = document.getElementsByName('paysera_payment_radio');

    for (let i = 0; i < payseraPaymentRadio.length; i++) {
        payseraPaymentRadio[i].addEventListener('change', function () {
            document.getElementById('paysera_payment_method').value = this.value;
        });
    }
});

function payseraCountryChange() {
    let payseraMethods = document.getElementsByClassName('paysera-methods');

    for (let i = 0; i < payseraMethods.length; i++) {
        payseraMethods[i].style.display = 'none';
    }

    let country = document.getElementById('paysera_payment_country').value;

    document.getElementById('paysera_payment_country_' + country).style.display = 'block';
}
